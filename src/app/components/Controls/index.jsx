import React from 'react';
import PropTypes from 'prop-types';
import style from './style.scss';

const Controls = props => {
	const { addUser } = props;
	return (
		<div className={style.controls}>
			<button
				className={style.addTransactionButton}
				onClick={() => addUser()}
			>
				Add new user
			</button>
		</div>
	);
};
Controls.propTypes = {
	addTransaction: PropTypes.func
};
export default Controls;
