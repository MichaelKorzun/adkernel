import React from 'react';
import { NavLink, Link } from 'react-router-dom';
import style from './style.scss';
import logo from 'app/assets/img/logo.png';

const NavMenu = () => {
	return (
		<div className={style.navMenu}>
			<div className={style.navMenuWrap}>
				<Link to="/" className={style.logoWrap}>
					<img className={style.logo} src={logo} />{' '}
				</Link>
				<NavLink to="/userslist" activeClassName={style.active}>
					Users list
				</NavLink>
			</div>
		</div>
	);
};

export default NavMenu;
