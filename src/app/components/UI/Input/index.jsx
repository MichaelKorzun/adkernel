import React, { Component } from 'react';
import PropTypes from 'prop-types';
import style from './style.scss';

class Input extends Component {
	static contextTypes = {
		isDay: PropTypes.string
	};

	componentDidMount() {
		this.props.input.value && this.Placeholder.classList.add('focus');
	}

	componentDidUpdate() {
		this.props.input.value && this.Placeholder.classList.add('focus');
	}

	onFocusHandler = () => {
		this.Placeholder.classList.add('focus');
	};

	render() {
		const {
			input,
			label,
			type,
			text,
			meta: { touched, error }
		} = this.props;

		return (
			<div className={style.inputComponentWrap}>
				<p className={style.description}>{text}</p>
				<div className={style.inputTextWrapper}>
					<input
						{...input}
						placeholder={label}
						type={type}
						onFocus={this.onFocusHandler}
						className={`${style.inputText} ${
							error && touched ? style.error : null
						}`}
					/>
					<span
						ref={el => (this.Placeholder = el)}
						className={style.placeholder}
					>
						{text}
					</span>
				</div>
				{touched && error ? (
					<span className={style.errorMsg}>{error}</span>
				) : null}
			</div>
		);
	}
}

Input.propTypes = {
	text: PropTypes.string
	, elements: PropTypes.array
	, input: PropTypes.object
	, meta: PropTypes.object
	, touched: PropTypes.bool
	, error: PropTypes.string
	, label: PropTypes.string
	, type: PropTypes.string
};

export default Input;
