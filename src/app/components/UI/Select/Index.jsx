import React from 'react';
import PropTypes from 'prop-types';
import style from './style.scss';

const Select = props => {
	const {
		text,
		elements,
		input,
		meta: { touched, error }
	} = props;
	return (
		<div className={style.select}>
			<p className={style.description}>{text}</p>
			<select
				className={`${style.select} ${error && touched ? style.error : null}`}
				{...input}
			>
				<option value="" />
				{elements.length &&
					elements.map((item, index) => (
						<option key={index} value={item}>
							{item}
						</option>
					))}
			</select>
		</div>
	);
};

Select.propTypes = {
	text: PropTypes.string
	, elements: PropTypes.array
	, input: PropTypes.object
	, meta: PropTypes.object
	, touched: PropTypes.bool
	, error: PropTypes.string
};
export default Select;
