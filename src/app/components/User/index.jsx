import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, formValueSelector, initialize } from 'redux-form';
import { connect } from 'react-redux';
import Input from 'app/components/UI/Input';
import Select from 'app/components/UI/Select';
import {
	validateNumber,
	validateRequire,
	validatePhonenumber,
	normalize
} from 'app/helpers/validations.js';
import {
	selectors as statsSelectors,
	newUser,
	editUserByIndex
} from 'app/ducks/stats.js';
import style from './style.scss';

const formInitialValues = {
	firstName: ''
	, lastName: ''
	, phone: ''
	, gender: ''
	, age: ''
};

class User extends Component {
	handleFormSubmit = values => {
		const { dispatch, userIndexForEdit } = this.props;
		if (userIndexForEdit < 0) {
			dispatch(newUser(values));
			dispatch(initialize('User', formInitialValues));
		} else {
			dispatch(
				editUserByIndex({
					user: values
					, index: userIndexForEdit
				})
			);
		}
	};

	componentDidMount() {
		const { dispatch, userIndexForEdit, userList } = this.props;
		dispatch(
			initialize(
				'User',
				userIndexForEdit >= 0 ? userList[userIndexForEdit] : formInitialValues
			)
		);
	}

	componentWillReceiveProps(nextProps) {
		const { userIndexForEdit, userList, dispatch } = this.props;
		if (userIndexForEdit !== nextProps.userIndexForEdit) {
			dispatch(initialize('User', userList[nextProps.userIndexForEdit]));
		}
	}

	render() {
		const { closeUserField, handleSubmit, userIndexForEdit } = this.props;
		return (
			<div className={style.transaction}>
				<form
					className={style.transactionForm}
					onSubmit={handleSubmit(this.handleFormSubmit)}
				>
					<div className={style.row}>
						<Field
							name="firstName"
							component={Input}
							type="text"
							text="First name"
							validate={validateRequire}
						/>
						<Field
							name="lastName"
							component={Input}
							type="text"
							text="Last name"
							validate={validateRequire}
						/>
					</div>
					<div className={style.row}>
						<Field
							name="phone"
							component={Input}
							type="text"
							text="Phone number"
							validate={validatePhonenumber}
						/>
						<Field
							name="age"
							component={Input}
							type="number"
							text="Age"
							validate={validateNumber}
							normalize={normalize}
						/>
					</div>
					<div className={style.row}>
						<Field
							name="gender"
							component={Select}
							elements={['Man', 'Woman']}
							type="select"
							text="Gender"
							validate={validateRequire}
						/>
						<div className={style.buttonWrap}>
							<button className={style.addTransactionButton} type="submit">
								{userIndexForEdit < 0 ? 'Add User' : 'Edit User'}
							</button>
						</div>
					</div>
				</form>
				<button
					className={style.closeTransactionField}
					onClick={() => closeUserField()}
				>
					{' '}
					X{' '}
				</button>
			</div>
		);
	}
}

User.propTypes = {
	dispatch: PropTypes.func
	, handleSubmit: PropTypes.func
	, closeUserField: PropTypes.func
	, userList: PropTypes.array
	, userIndexForEdit: PropTypes.number
};

const mapStateToProps = state => {
	return {
		userList: statsSelectors.getUsersList(state)
	};
};

export default connect(mapStateToProps)(reduxForm({ form: 'User' })(User));
