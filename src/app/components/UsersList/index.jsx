import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
	selectors as statsSelectors,
	deleteUserByIndex,
	sortUserList
} from 'app/ducks/stats';
import Controls from 'app/components/Controls';
import User from 'app/components/User';
import style from './style.scss';

class UsersList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showAddTransactionField: false
			, userIndexForEdit: -1
			, sortDir: true
		};
	}

	handleDeleteUser = (_, userIndex) => {
		const { dispatch } = this.props;
		dispatch(deleteUserByIndex(userIndex));
	};

	handleEditUser = (_, transactionIndex) => {
		this.setState({
			userIndexForEdit: transactionIndex
			, showAddTransactionField: true
		});
	};

	userField = () => {
		this.setState(prevState => ({
			userIndexForEdit: -1
			, showAddTransactionField: !prevState.showAddTransactionField
		}));
	};

	handleSort = (_, item) => {
		const { dispatch } = this.props;
		dispatch(sortUserList({ param: item, direction: this.state.sortDir }));
		this.setState(prevState => {
			return { sortDir: !prevState.sortDir };
		});
	};

	render() {
		const { showAddTransactionField, userIndexForEdit } = this.state;
		const { userList } = this.props;
		return (
			<div className={style.transactions}>
				<Controls addUser={this.userField} />
				{showAddTransactionField && (
					<User
						closeUserField={this.userField}
						userIndexForEdit={userIndexForEdit}
					/>
				)}
				<ul className={style.transactionsList}>
					<li className={`${style.transaction} ${style.transactionTitle}`}>
						<span>
							First name<i onClick={e => this.handleSort(e, 'firstName')} />
						</span>
						<span>
							Last name<i onClick={e => this.handleSort(e, 'lastName')} />
						</span>
						<span>
							Phone<i onClick={e => this.handleSort(e, 'phone')} />
						</span>
						<span>
							Gender<i onClick={e => this.handleSort(e, 'gender')} />
						</span>
						<span>
							Age<i onClick={e => this.handleSort(e, 'age')} />
						</span>
					</li>
					{userList.length
						? userList.map((item, index) => {
							return (
								<li
									className={`${style.transaction} ${style.transactionData}`}
									key={index}
								>
									<span>{item.firstName}</span>
									<span>{item.lastName}</span>
									<span>{item.phone}</span>
									<span>{item.gender}</span>
									<span>{item.age}</span>
									<div className={style.buttonsWrap}>
										<button
											className={style.edit}
											onClick={e => this.handleEditUser(e, index)}
										/>
										<button
											className={style.delete}
											onClick={e => this.handleDeleteUser(e, index)}
										/>
									</div>
								</li>
							);
						  })
						: null}
				</ul>
			</div>
		);
	}
}

UsersList.propTypes = {
	dispatch: PropTypes.func
	, userList: PropTypes.array
};

const mapStateToProps = state => {
	return {
		userList: statsSelectors.getUsersList(state)
	};
};

export default connect(mapStateToProps)(UsersList);
