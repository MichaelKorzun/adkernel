export const ADD_NEW_USER = 'ADD_NEW_USER';
export const ADD_USER_LIST = 'ADD_USER_LIST';
export const ADD_NEW_TRANSACTION_CATEGORY = 'ADD_NEW_TRANSACTION_CATEGORY';
export const DELETE_USER = 'DELETE_USER';
export const EDIT_USER = 'EDIT_USER';
export const SORT_USERS = 'SORT_USERS';

const initialState = {
	usersList: []
};

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case ADD_NEW_USER:
			return {
				...state
				, usersList: [...state.usersList, payload]
			};

		case ADD_USER_LIST:
			return {
				...state
				, usersList: payload
			};

		case DELETE_USER:
			return {
				...state
				, usersList: state.usersList.filter((item, index) => index !== payload)
			};

		case EDIT_USER: {
			return {
				...state
				, usersList: [
					...state.usersList.slice(0, payload.index)
					, payload.user
					, ...state.usersList.slice(payload.index + 1)
				]
			};
		}

		case SORT_USERS: {
			if (payload.direction) {
				return {
					...state
					, usersList: state.usersList.slice().sort((a, b) => {
						if (a[payload.param] > b[payload.param]) return 1;
						if (a[payload.param] < b[payload.param]) return -1;
						return 0;
					})
				};
			} else {
				return {
					...state
					, usersList: state.usersList.slice().sort((a, b) => {
						if (a[payload.param] < b[payload.param]) return 1;
						if (a[payload.param] > b[payload.param]) return -1;
						return 0;
					})
				};
			}
		}

		default:
			return state;
	}
};

export const addNewUser = payload => ({
	type: ADD_NEW_USER
	, payload
});

export const addUsersList = payload => ({
	type: ADD_USER_LIST
	, payload
});

export const deleteUser = payload => ({
	type: DELETE_USER
	, payload
});

export const editUser = payload => ({
	type: EDIT_USER
	, payload
});

export const sortUsers = payload => ({
	type: SORT_USERS
	, payload
});

export const setInitialStateFromLS = initialState => dispatch => {
	dispatch(addUsersList(initialState));
};

export const newUser = user => dispatch => {
	dispatch(addNewUser(user));
};

export const deleteUserByIndex = userIndex => dispatch => {
	dispatch(deleteUser(userIndex));
};

export const editUserByIndex = userData => dispatch => {
	dispatch(editUser(userData));
};

export const sortUserList = sortParameter => dispatch => {
	dispatch(sortUsers(sortParameter));
};

export const selectors = {
	getUsersList: state => state.stats.usersList
};
