export const validateNumber = value => {
	let error;
	if (!value) {
		error = 'Required';
	} else if (value < 1) {
		error = 'Must be 1 or more';
	} else if (value > 99) {
		error = 'Must be less then 100';
	}
	return error;
};

export const validateRequire = value => {
	let error;
	if (!value) {
		error = 'Required';
		return error;
	}
};

export const validatePhonenumber = value => {
	let error;
	if (!/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{6})/.test(value)) {
		error = '+38067000000000';
	} else if (value && value.length > 13) error = '+38067000000000';

	return error;
};

export const normalize = value => +value;
