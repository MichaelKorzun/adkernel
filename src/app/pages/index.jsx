import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
	selectors as statsSelectors,
	setInitialStateFromLS
} from 'app/ducks/stats';
import UsersList from 'app/components/UsersList';
import NavMenu from 'app/components/NavMenu';
import Welcome from 'app/components/Welcome';

//MOC
const mocData = {
	users: [
		{
			firstName: 'Ivan'
			, lastName: 'Navi'
			, phone: '+380671111111'
			, gender: 'Man'
			, age: 14
		}
		, {
			firstName: 'Alina'
			, lastName: 'Anila'
			, phone: '+380672222222'
			, gender: 'Woman'
			, age: 88
		}
	]
};

class App extends React.Component {
	componentDidMount() {
		const { dispatch } = this.props;
		const dataFromLS = localStorage.getItem('initialState');
		if (dataFromLS) {
			dispatch(setInitialStateFromLS(JSON.parse(dataFromLS).users));
		} else {
			localStorage.setItem('initialState', JSON.stringify(mocData));
			dispatch(setInitialStateFromLS(mocData.users));
		}
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.userList !== this.props.userList) {
			localStorage.clear();
			localStorage.setItem(
				'initialState',
				JSON.stringify({
					users: nextProps.userList
				})
			);
		}
	}

	render() {
		return (
			<BrowserRouter>
				<React.Fragment>
					<NavMenu />
					<Switch>
						<Route
							exact
							path="/"
							component={() => <Welcome {...this.props} />}
						/>
						<Route exact path="/userslist" component={UsersList} />
					</Switch>
				</React.Fragment>
			</BrowserRouter>
		);
	}
}

App.propTypes = {
	dispatch: PropTypes.func
	, userList: PropTypes.array
};

const mapStateToProps = state => {
	return {
		userList: statsSelectors.getUsersList(state)
	};
};

export default connect(mapStateToProps)(App);
