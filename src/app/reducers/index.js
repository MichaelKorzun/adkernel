import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import stats from 'app/ducks/stats';

export default combineReducers({
	stats
	, form: formReducer
});
