
module.exports = ({ isProd, sourcePath }) => ([
	// {
	// 	loader: 'style-loader'
	// }
	 {
		loader: 'css-loader'
		, options: {
			module: false
			, localIdentName: '[path][name]-[local]'
			, sourceMap: !isProd
		}
	}
	, {
		loader: 'postcss-loader'
		, options: {
			sourceMap: !isProd
			, browsers: ['last 3 version', 'ie >= 10']
		}
	}
	, {
		loader: 'sass-loader'
		, options: {
			outputStyle: isProd ? 'compressed' : 'expanded'
			, sourceMap: !isProd
			, includePaths: [sourcePath]
			, data: ['@import "app/variables.scss";'].join()
		}
	}
]);
