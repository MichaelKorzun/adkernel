const webpack = require('webpack');

module.exports = () => ([
	new webpack.ContextReplacementPlugin(/moment\/locale$/, /^\.\/(en|ru)$/) //load only en&ru locales in moment.js
]);
