const path = require('path');
const htmlWebpackPlugin = require('html-webpack-plugin');

module.exports = ({ isProd, sourcePath }) => [
	new htmlWebpackPlugin({
		template: path.resolve(sourcePath, './index.html')
		, inject: true
		, production: isProd
		, minify: isProd && {
			removeComments: true
			, collapseWhitespace: true
			, removeRedundantAttributes: true
			, useShortDoctype: true
			, removeEmptyAttributes: true
			, removeStyleLinkTypeAttributes: true
			, keepClosingSlash: true
			, minifyJS: true
			, minifyCSS: true
			, minifyURLs: true
		}
	})
];
