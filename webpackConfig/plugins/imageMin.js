const ImageminPlugin = require('imagemin-webpack-plugin').default;

module.exports = ({ isProd }) => {

	return (isProd
		? ([
			new ImageminPlugin({
				pngquant: {
					quality: '95-100'
				}
			})
		])
		: []
	);
};
